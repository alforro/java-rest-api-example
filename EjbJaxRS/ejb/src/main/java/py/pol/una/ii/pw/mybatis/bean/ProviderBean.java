/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.mybatis.bean;


public class ProviderBean {

    private Long id;

    private String name;

    private String phone_number;

    private String email;

    private String ruc;
    
    public ProviderBean(){
    	
    }

    public ProviderBean(Long id, String name, String phone_number, String email, String ruc) {
		super();
		this.id = id;
		this.name = name;
		this.phone_number = phone_number;
		this.email = email;
		this.ruc = ruc;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.phone_number = PhoneNumber;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    
    public void setProvider(ProviderBean provider){
    	this.id=provider.getId();
    	this.name = provider.getName();
    	this.email = provider.getEmail();
    	this.ruc = provider.getRuc();
    	this.phone_number = provider.getPhoneNumber();
    }
    

}
