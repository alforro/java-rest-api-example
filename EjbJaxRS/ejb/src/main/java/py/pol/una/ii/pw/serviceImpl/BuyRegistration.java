/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.BuyBean;
import py.pol.una.ii.pw.mybatis.bean.BuyDetailBean;
import py.pol.una.ii.pw.mybatis.bean.ProductBean;
import py.pol.una.ii.pw.mybatis.bean.ProviderBean;
import py.pol.una.ii.pw.mybatis.manager.BuyDetailManager;
import py.pol.una.ii.pw.mybatis.manager.BuyManager;
import py.pol.una.ii.pw.mybatis.manager.ProductManager;
import py.pol.una.ii.pw.mybatis.manager.ProviderManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * 
 * @author gmarin
 *
 */

@Stateful
@SessionScoped
@TransactionManagement(TransactionManagementType.BEAN)
public class BuyRegistration {

   	@Resource
   	private TransactionManager transaction;
   	
   	@Inject
    private Logger log;

    @EJB
    private BuyManager buyManager;
    
    @EJB
    private BuyDetailManager buyDetailManager;
    
    @EJB
    private ProductManager productManager;
    
    @EJB
    private ProviderManager providerManager;
       
    private Transaction ts;
       
    private BuyBean buy = new BuyBean();

    /**
     * Iniciar compra por proveedor
     * 
     * @param provider
     */
   	public void initBuy(ProviderBean provider){
   		
   		try {
   			transaction.begin();
   	    	
   	    	this.buy.setDate(new Date());
   	    	this.buy.setTotal(0.0F);
   	    	this.buy.setProviderId(provider.getId());
   	        
   	    	buyManager.newBuy(buy);
   	        
   		} catch (NotSupportedException e) {
   			e.printStackTrace();
   			
   		} catch (SystemException e) {
   			e.printStackTrace();
   			
   		}
   		
   		try {
   			ts = transaction.suspend();
   			
   		} catch (SystemException e) {
   			e.printStackTrace();
   			
   		}
   	}
   	
   	/**
   	 * Agregar items a la compra
   	 * 
   	 * @param buyDetailList
   	 * @throws Exception
   	 */
   	public void addItemBuy(List<BuyDetailBean> buyDetailList) throws Exception{
    	
    	Float total = 0.0F;
    	
    	transaction.resume(ts);
    	
    	
    	for(BuyDetailBean buyDetail: buyDetailList){

            log.info("Registrando detalle de compra con idProducto " + buyDetail.getProductId());
            
            BuyDetailBean newBuyDetail = new BuyDetailBean();
            
            newBuyDetail.setProductId(buyDetail.getProductId());
            newBuyDetail.setQuantity(buyDetail.getQuantity());
            newBuyDetail.setBuyId(this.buy.getId());
            
        	buyDetailManager.newBuyDetail(newBuyDetail);
            
        	ProductBean product= productManager.getProductById(newBuyDetail.getProductId());
        	
        	Integer quantity = product.getStock() + newBuyDetail.getQuantity();
        	total = (float) (total +(newBuyDetail.getQuantity()*product.getSalePrice()));
        	
        	product.setStock(quantity);
        	
        	productManager.updateProduct(product);
        	                   
        }
    	
    	this.buy.setTotal(total);
    	
    	buyManager.updateBuy(buy);
    	
    	ts = transaction.suspend();
    	
    }
    
   	/**
   	 * Actualizar datos de la compra
   	 * 
   	 * @param buy_mod
   	 */
    public void updateBuy(BuyBean buy_mod) {
    	
    	log.info("Actualizando compra con id " + buy_mod.getId());
    	
    	BuyBean buy = buyManager.getBuyById(buy_mod.getId());
    	
    	buy.setDate(buy_mod.getDate());
    	buy.setProviderId(buy_mod.getProviderId());
    	buy.setTotal(buy_mod.getTotal());
    	
    	buyManager.updateBuy(buy);
    	
    	log.info("Se ha actualizado compra con id " + buy.getId());
    	
    }
     
    /**
     * Eliminar items de compra
     * @param idBuyDetails
     * @throws Exception
     */
    public void deleteItemBuy(List<Long> idBuyDetails) throws Exception{
    	    	
    	transaction.resume(ts);
    	
    	Float total = this.buy.getTotal();
    	
    	for(Long idBuyDetail: idBuyDetails){

            log.info("Eliminando item " + idBuyDetail);
          
            BuyDetailBean bd = buyDetailManager.getBuyDetailById(idBuyDetail);
            
            ProductBean product= productManager.getProductById(bd.getProductId());
        	
        	Integer quantity = product.getStock() - bd.getQuantity();
        	total = (float) (total -(bd.getQuantity()*product.getSalePrice()));
        	
        	product.setStock(quantity);
        	
        	productManager.updateProduct(product);
      	    
        	buyDetailManager.deleteBuyDetail(idBuyDetail);
        	                   
        }
    	
    	this.buy.setTotal(total);
    	
    	buyManager.updateBuy(buy);
    	
    	ts = transaction.suspend();
    	
    }
    
    /**
     * Confirmar compra
     * 
     * @return
     */
    public BuyBean confirmBuy(){
    	try {
			transaction.resume(ts);
		
    	} catch (InvalidTransactionException e) {
			e.printStackTrace();
		
		} catch (IllegalStateException e) {
			e.printStackTrace();
		
		} catch (SystemException e) {
			e.printStackTrace();
		
		}
    	
    	try {
    		
        	buyManager.updateBuy(buy);
			transaction.commit();
			
		} catch (SecurityException e) {
			e.printStackTrace();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
		} catch (RollbackException e) {
			e.printStackTrace();
			
		} catch (HeuristicMixedException e) {
			e.printStackTrace();
			
		} catch (HeuristicRollbackException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();

		}
    	
    	return this.buy;
    	
    }
    
    /**
     * Cancelar compra
     * 
     */
    public void cancelBuy(){
    	try {
			transaction.resume(ts);
		
    	} catch (InvalidTransactionException e) {
			e.printStackTrace();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();
			
		}
    	
    	try {
			transaction.rollback();
		} catch (SecurityException e) {
			e.printStackTrace();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();
			
		}
    	
    }
 
    @PostConstruct
    public void postConstruct() throws NamingException{
	
		InitialContext ctx = new InitialContext();
		try {
			transaction= (TransactionManager) ctx.lookup("java:/TransactionManager");
			
		} catch (Exception e) {
		}
    }
    
   
}