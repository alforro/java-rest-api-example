package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.ibatis.session.SqlSession;
import py.pol.una.ii.pw.mybatis.bean.BuyBean;
import py.pol.una.ii.pw.mybatis.mapper.BuyMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class BuyManager implements BuyMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
        	BuyMapper buyMapper = sqlSession.getMapper(BuyMapper.class);
            exist = buyMapper.getBuyById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newBuy(BuyBean buy) {
        
        try {
        	BuyMapper buyMapper = sqlSession.getMapper(BuyMapper.class);
            
        	buyMapper.newBuy(buy);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return buy.getId();
    }
    public BuyBean getBuyById(Long id) {
        
    	BuyBean buy = null;
        try {
        	BuyMapper buyMapper = sqlSession.getMapper(BuyMapper.class);
        	buy = buyMapper.getBuyById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (buy);
    }
   
    public List<BuyBean> getAllBuys(){
        
        List<BuyBean> buys = null;
        try {
        	BuyMapper buyMapper = sqlSession.getMapper(BuyMapper.class);
            buys = buyMapper.getAllBuys();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (buys);
    }
    public Long updateBuy(BuyBean buy_mod){
        
        Long id_buy = null;
        try {
        	BuyMapper buyMapper = sqlSession.getMapper(BuyMapper.class);
            id_buy = buyMapper.updateBuy(buy_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_buy;
    }
    
    public void deleteBuy(Long id){
        try {
        	BuyMapper buyMapper = sqlSession.getMapper(BuyMapper.class);
        	buyMapper.deleteBuy(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    

    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
	
}
