package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.SessionBean;
import py.pol.una.ii.pw.mybatis.mapper.SessionMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class SessionManager implements SessionMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
            SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            exist = sessionMapper.getSessionById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newSession(SessionBean user) {
        
        try {
            SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            
            sessionMapper.newSession(user);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return user.getId();
    }
    public SessionBean getSessionById(Long id) {
        
        SessionBean session = null;
        try {
            SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            session = sessionMapper.getSessionById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (session);
    }
  
    public List<SessionBean> findAllOrderedByName(){
        
        List<SessionBean> sessions = null;
        try {
            SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            sessions = sessionMapper.getAllSessions();
            
            //Collections.sort(clientes);//, (a, b) -> a.getName().compareToIgnoreCase(b.getName()));
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (sessions);
    }
    public List<SessionBean> getAllSessions(){
        
        List<SessionBean> sessions = null;
        try {
            SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            sessions = sessionMapper.getAllSessions();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (sessions);
    }
    
    public Long updateSession(SessionBean session_mod){
        
        Long id_session = null;
        try {
            SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            id_session = sessionMapper.updateSession(session_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_session;
    }
    
    public void deleteSession(Long id){
        try {
            SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            sessionMapper.deleteSession(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    public SessionBean getSessionByToken(String token) {
	 
    	SessionBean session = null;
    	
        try {
        	SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
        	
        	session = sessionMapper.getSessionByToken(token);
        	
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (session);
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}
