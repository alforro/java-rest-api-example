package py.pol.una.ii.pw.serviceImpl;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import py.pol.una.ii.pw.mybatis.bean.BuyBean;
import py.pol.una.ii.pw.mybatis.bean.BuyDetailBean;
import py.pol.una.ii.pw.mybatis.bean.ProductBean;
import py.pol.una.ii.pw.mybatis.manager.BuyDetailManager;
import py.pol.una.ii.pw.mybatis.manager.BuyManager;
import py.pol.una.ii.pw.mybatis.manager.ProductManager;
import py.pol.una.ii.pw.mybatis.manager.ProviderManager;
import py.pol.una.ii.pw.util.FileException;

/**
 * 
 * @author gmarin
 *
 */

@Stateless
public class MassiveBuyRegistration {
	
   	
   	 @Inject
     private Logger log;
   	 
     @Resource
     private SessionContext context;

     @EJB
     private BuyManager buyManager;
     
     @EJB
     private ProviderManager providerManager;
     
     @EJB
     private ProductManager productManager;
     
     @EJB
     private BuyDetailManager buyDetailManager;
      
     /**
      * Crear compras masivas a partir de un archivo que es recibido como parámetro.
      * Cada linea del archivo es de la sgte forma:
      * provider=#num;product=#num-quantity=#num
      * En donde la primera columna corresponde al id del proveedor,
      * Segunda columna id del producto,
      * Tercera columna cantidad del producto a comprar.
      * 
      * @param fr
      * @throws IOException
      * @throws FileException
      */
     @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public void massiveBuyRegistration( FileReader fr) throws IOException, FileException{

	        boolean rollback = false;
	        HashMap<String,String> exceptions = new HashMap<String,String>(); 
	        BufferedReader br = new BufferedReader(fr);
	        exceptions.clear();
	        String line;


	        try {
		        
	            while ( ( line = br.readLine() ) != null ){
	                	            	
	            	log.info("Line: " + line);
	                
	                try {
	                    createBuy(line);

	                } catch (FileException e) {
	                    exceptions.put( "line ",  e.getMessage() );
	                    rollback = true;

	                } catch (ConstraintViolationException e) {
	                    exceptions.put( "line ",  e.getConstraintViolations().toString() );
	                    rollback = true;
	                } catch (Exception e) {
	                    exceptions.put( "line ",  e.getMessage() );
	                    rollback = true;
	                }
	            }
	        } catch (IOException e) {
	            throw new IOException();
	        }

	        if( rollback ){
	            context.setRollbackOnly();
	        }

	    }
	    
     /**
      * Crear una compra nueva
      * 
      * @param b
      * @return
      * @throws Exception
      * @throws ConstraintViolationException
      */
	 private BuyBean createBuy(String b) throws Exception, ConstraintViolationException{

	    	//Convertir linea a una lista de cadenas
	    	BuyBean buy = new BuyBean();
	        String[] p = b.split(";");
	        
	        for (String logInfo : p) {
	        	
	        	log.info("Split para separar compra y detalle: " + logInfo);
				
			}
	        
            String idProviderString= p[0].substring(9);
            Long idProvider = Long.parseLong(idProviderString);
            
            log.info("Provider id " + idProvider);
            
	        buy.setProviderId(idProvider);
	        buy.setDate(new Date());
	        
	        buyManager.newBuy(buy);
	        
	        log.info("Buy id " + buy.getId() + "registred");
	        
	        createBuyDetails(buy, p[1]);
	        	        
	        return buy;
	 }
	    
	 /**
	  * Crear detalle de compra
	  * 
	  * @param buy
	  * @param buyDetail
	  */
	 private void createBuyDetails(BuyBean buy, String buyDetail){

	        String[] d = buyDetail.split(",");
	        
	        for (String logInfo : d) {
	        	
	        	log.info("Split para separar detalles: " + logInfo);
				
			}
	        Float total = 0.0F;

	        for( String details: d){
	        	
	            BuyDetailBean newBuyDetail = new BuyDetailBean();
	            
	        	//Se separan los productos y cantidades
	        	String[] propDetails = details.split("-");
	        	for (String logInfo : propDetails) {
		        	
		        	log.info("Split para separar datos de detalles: " + logInfo);
					
				}
	        	
	        	//Se obtiene el id del producto
	        	String idProductString= propDetails[0].substring(8);
	        	
	        	//Se obtiene la cantidad
	        	String quantity= propDetails[1].substring(9);
	        			    	
	            Long idProduct = Long.parseLong(idProductString);
	            log.info("Product id " + idProduct);
	            	            
	            newBuyDetail.setProductId(idProduct);
	            newBuyDetail.setQuantity(Integer.parseInt(quantity));
	            newBuyDetail.setBuyId(buy.getId());
	            
	        	buyDetailManager.newBuyDetail(newBuyDetail);
	        	
	        	ProductBean product = productManager.getProductById(idProduct);
	            	        	
	        	Integer qt = product.getStock() + newBuyDetail.getQuantity();
	        	total = (float) (total +(newBuyDetail.getQuantity()*product.getBuyPrice()));
	        	
	        	product.setStock(qt);
		        	                   
		    }
		    	
		    buy.setTotal(total);

		    buyManager.updateBuy(buy);
	 }
	 
	 /**
	  * Listar todas las compras existentes.
	  * 
	  * @return
	  * @throws Exception
	  */
	 @TransactionAttribute(TransactionAttributeType.REQUIRED)
	 public List<BuyBean> findAll(){
	    	
	    	log.info("Buscando todas las compras existentes");
	    	
	    	List<BuyBean> list = buyManager.getAllBuys();
	    	
	    	log.info("Se han obtenido " + list.size() + " compras.");
	    	
	    	return list;
	 }
	 
	 /**
	  * Buscar compra por id
	  * 
	  * @param id
	  * @return
	  */
	 @TransactionAttribute(TransactionAttributeType.REQUIRED)
     public BuyBean findById(Long id){
	    	
	    	log.info("Buscando compra con id " + id);
	    	
	    	BuyBean buy = buyManager.getBuyById(id);
	    	
	    	log.info("Se ha obtenido compra con id " + buy.getId());
	    	
	    	return buy;
	 }
	 
	 /**
      * Actualizar datos de compra
      * 
      * @param buy_mod
      */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateBuy(BuyBean buy_mod){
    	
    	log.info("Actualizando compra con id " + buy_mod.getId());
    	
    	BuyBean buy = buyManager.getBuyById(buy_mod.getId());
    	
    	buy.setDate(buy_mod.getDate());
    	buy.setProviderId(buy_mod.getProviderId());
    	buy.setTotal(buy_mod.getTotal());
    	
    	buyManager.updateBuy(buy);
    	
    	log.info("Se ha actualizado compra con id " + buy.getId());
    }
    
    /**
     * Eliminar compra por id
     * 
     * @param id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteBuy(Long id){
    	log.info("Eliminando compra con id " + id);
    	
    	buyManager.deleteBuy(id);;
    	
  	  	log.info("Se ha eliminado compra con id" + id);
    }
}
