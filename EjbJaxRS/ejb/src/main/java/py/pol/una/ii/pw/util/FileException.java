package py.pol.una.ii.pw.util;

public class FileException extends Exception{
	
	private String message;

    public FileException( String message ){
        this.message = message;
    }

    @Override
    public String getMessage(){
        return this.message;
    }


}
