/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.ClientBean;
import py.pol.una.ii.pw.mybatis.bean.PaymentBean;
import py.pol.una.ii.pw.mybatis.manager.ClientManager;
import py.pol.una.ii.pw.mybatis.manager.PaymentManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class PaymentRegistration {

    @Inject
    private Logger log;
    
    @Inject
    private ClientManager clientManager;
    
    @EJB
    private PaymentManager paymentManager;
    
    /**
     * Crear nuevo pago
     * 
     * @param payment
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createPayment(PaymentBean payment) {
        
    	log.info("Registering payment for client with id" + payment.getClientId());
    	
    	PaymentBean newPayment = new PaymentBean();
    	
    	newPayment.setClientId(payment.getClientId());
    	newPayment.setDate(payment.getDate());
    	newPayment.setTotal(payment.getTotal());
        
        paymentManager.newPayment(newPayment);
        
        ClientBean client = clientManager.getClientById(newPayment.getClientId());
        
        //Se resta la deuda del cliente
        float totalDue = client.getDue() - newPayment.getTotal();
        
        client.setDue(totalDue);
        
        clientManager.updateClient(client);
        
    }
   
    /**
     * Actualizar datos de pago
     * 
     * @param payment_mod
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updatePayment(PaymentBean payment_mod){
    	
    	log.info("Actualizando pago con id " + payment_mod.getId());
    	
    	PaymentBean payment = paymentManager.getPaymentById(payment_mod.getId());
    	
    	payment.setClientId(payment_mod.getClientId());
    	payment.setDate(payment_mod.getDate());
    	payment.setTotal(payment_mod.getTotal());
    	
    	paymentManager.updatePayment(payment);
    	
    	log.info("Se ha actualizado pago con id " + payment.getId());
    }
    
    /**
     * Eliminar pago por id
     * 
     * @param id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deletePayment(Long id){
    	
    	log.info("Eliminando pago con id " + id);
    	
    	paymentManager.deletePayment(id);;
    	
  	  	log.info("Se ha eliminado pago con id" + id);
    }
    
    /**
     * Listar todos los pagos existentes
     * 
     * @return list
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<PaymentBean> findAll(){
    	
    	log.info("Buscando todos los pagos existentes");
    	
    	List<PaymentBean> list = paymentManager.getAllPayments();
    	
    	log.info("Se han obtenido " + list.size() + " pagos.");
    	
    	return list;
    }
    
    /**
     * Buscar pago por id
     * 
     * @param id
     * @return payment
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public PaymentBean findById(Long id){
    	
    	log.info("Buscando producto con id " + id);
    	
    	PaymentBean payment = paymentManager.getPaymentById(id);
    	
    	log.info("Se ha obtenido pago con id " + payment.getId());
    	
    	return payment;
    }
    

}