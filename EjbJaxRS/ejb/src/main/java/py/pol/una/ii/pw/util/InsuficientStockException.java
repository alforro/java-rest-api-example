package py.pol.una.ii.pw.util;

public class InsuficientStockException extends Exception{

    private String message;

    public InsuficientStockException( String message ){
        this.message = message;
    }

    @Override
    public String getMessage(){
        return this.message;
    }
}
