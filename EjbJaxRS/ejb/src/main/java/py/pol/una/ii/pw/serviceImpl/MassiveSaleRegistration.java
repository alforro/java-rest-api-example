package py.pol.una.ii.pw.serviceImpl;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import py.pol.una.ii.pw.mybatis.bean.ProductBean;
import py.pol.una.ii.pw.mybatis.bean.SaleBean;
import py.pol.una.ii.pw.mybatis.bean.SaleDetailBean;
import py.pol.una.ii.pw.mybatis.manager.ClientManager;
import py.pol.una.ii.pw.mybatis.manager.ProductManager;
import py.pol.una.ii.pw.mybatis.manager.ProviderManager;
import py.pol.una.ii.pw.mybatis.manager.SaleDetailManager;
import py.pol.una.ii.pw.mybatis.manager.SaleManager;
import py.pol.una.ii.pw.util.FileException;

/**
 * 
 * @author gmarin
 *
 */

@Stateless
public class MassiveSaleRegistration {
   	
   	 @Inject
     private Logger log;
   	 
     @Resource
     private SessionContext context;
     
     @EJB
     private SaleManager saleManager;
     
     @EJB
     private ClientManager clientManager;
     
     @EJB
     private ProductManager productManager;
     
     @EJB
     private ProviderManager providerManager;
     
     @EJB
     private SaleDetailManager saleDetailManager;
              	
     /**
      * Crear ventas masivas a partir de un archivo que es recibido como parámetro.
      * Cada linea del archivo es de la sgte forma:
      * client=#num;product= #num-quantity=#num
      * En donde la primera columna corresponde al id del cliente,
      * Segunda columna id del producto,
      * Tercera columna cantidad del producto a vender.
      * 
      * @param fr
      * @throws IOException
      * @throws FileException
      */
     @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public void massiveSaleRegistration( FileReader fr) throws IOException, FileException{

    	 	boolean rollback = false;
	        HashMap<String,String> exceptions = new HashMap<String,String>(); 
	        BufferedReader br = new BufferedReader(fr);
	        exceptions.clear();
	        String line;

	        try {
		        
	            while ( ( line = br.readLine() ) != null ){
	                	            	
	            	log.info("Line: " + line);
	                
	                try {
	                    createSale(line);

	                } catch (FileException e) {
	                    exceptions.put( "line ",  e.getMessage() );
	                    rollback = true;

	                } catch (ConstraintViolationException e) {
	                    exceptions.put( "line ",  e.getConstraintViolations().toString() );
	                    rollback = true;
	                } catch (Exception e) {
	                    exceptions.put( "line ",  e.getMessage() );
	                    rollback = true;
	                }
	            }
	        } catch (IOException e) {
	            throw new IOException();
	        }

	        if( rollback ){
	            context.setRollbackOnly();
	        }

	 }
	    
     /**
      * Crear venta de producto
      * 
      * @param s
      * @return sale
      * @throws Exception
      * @throws ConstraintViolationException
      */
	 private SaleBean createSale(String s) throws Exception, ConstraintViolationException{

	    	//Convertir linea a una lista de cadenas
	    	String[] c = s.split(";");
	        
	        for (String logInfo : c) {
	        	
	        	log.info("Split para separar venta y detalle: " + logInfo);
				
			}
	        
            String idClientString= c[0].substring(7);
            Long idClient = Long.parseLong(idClientString);
            
            log.info("Client id " + idClient);
            
	    	SaleBean sale = new SaleBean();
            
            sale.setClientId(idClient);
            sale.setDate(new Date());
            sale.setTotal(0.0F);
	        
	        Long newSale = saleManager.newSale(sale);
	        
	        log.info("Sale id " + newSale + "registred");
	        
	        createSaleDetails(newSale, c[1]);
	        	        
	        return sale;
	 }
	    
	 /**
	  * Crear detalle de venta
	  * 
	  * @param saleId
	  * @param saleDetail
	  */
	 private void createSaleDetails(Long saleId, String saleDetail){

	        String[] d = saleDetail.split(",");
	        
	        SaleBean sale = saleManager.getSaleById(saleId);
	        
	        for (String logInfo : d) {
	        	
	        	log.info("Split para separar detalles: " + logInfo);
				
			}
	        Float total = 0.0F;

	        for( String details: d){
	        	
	            SaleDetailBean newSaleDetail = new SaleDetailBean();
	            
	        	//Se separan los productos y cantidades
	        	String[] propDetails = details.split("-");
	        	for (String logInfo : propDetails) {
		        	
		        	log.info("Split para separar datos de detalles: " + logInfo);
					
				}
	        	
	        	//Se obtiene el id del producto
	        	String idProductString= propDetails[0].substring(8);
	        	Long idProduct = Long.parseLong(idProductString);
	        	
	        	//Se obtiene la cantidad
	        	String quantity= propDetails[1].substring(9);
	        			    	
	            log.info("Product id " + idProduct);
	            
	            ProductBean product = productManager.getProductById(idProduct);    
	            
	            newSaleDetail.setProductId(idProduct);
	            newSaleDetail.setQuantity(Integer.parseInt(quantity));
	            newSaleDetail.setSaleId(sale.getId());
	            
	        	saleDetailManager.newSaleDetail(newSaleDetail);
	            	        	
	        	//Integer qt = product.getStock() - newSaleDetail.getQuantity();
	        	total = (float) (total +(newSaleDetail.getQuantity()*product.getSalePrice()));
	        	
	        	//product.setStock(qt);
		        	                   
		    }
		    	
		    sale.setTotal(total);
		    saleManager.updateSale(sale);
		    		        	
	 }
	 
	 /**
	  * Listar todas las ventas existentes.
	  * 
	  * @return
	  * @throws Exception
	  */
	 @TransactionAttribute(TransactionAttributeType.REQUIRED)
	 public List<SaleBean> findAll(){
	    	
	    	log.info("Buscando todas las ventas existentes");
	    	
	    	List<SaleBean> list = saleManager.getAllSales();
	    	
	    	log.info("Se han obtenido " + list.size() + " ventas.");
	    	
	    	return list;
	 }
	 
	 /**
	  * Buscar venta por id
	  * 
	  * @param id
	  * @return
	  */
	 @TransactionAttribute(TransactionAttributeType.REQUIRED)
     public SaleBean findById(Long id){
	    	
	    	log.info("Buscando venta con id " + id);
	    	
	    	SaleBean sale = saleManager.getSaleById(id);
	    	
	    	log.info("Se ha obtenido venta con id " + sale.getId());
	    	
	    	return sale;
	 }
	 
	 /**
      * Actualizar datos de venta
      * 
      * @param sale_mod
      */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateSale(SaleBean sale_mod){
    	
    	log.info("Actualizando venta con id " + sale_mod.getId());
    	
    	SaleBean sale = saleManager.getSaleById(sale_mod.getId());
    	
    	sale.setClientId(sale_mod.getClientId());
    	sale.setDate(sale_mod.getDate());
    	sale.setTotal(sale_mod.getTotal());
    	
    	saleManager.updateSale(sale);
    	
    	log.info("Se ha actualizado venta con id " + sale.getId());
    }
    
    /**
     * Eliminar venta por id
     * 
     * @param id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteSale(Long id){
    	log.info("Eliminando venta con id " + id);
    	
    	saleManager.deleteSale(id);
    	
  	  	log.info("Se ha eliminado venta con id" + id);
    }
}
