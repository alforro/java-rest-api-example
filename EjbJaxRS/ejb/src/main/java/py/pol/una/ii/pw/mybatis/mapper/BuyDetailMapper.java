package py.pol.una.ii.pw.mybatis.mapper;
import java.util.List;

import py.pol.una.ii.pw.mybatis.bean.BuyDetailBean;



public interface BuyDetailMapper{

    public Boolean isExist(Long id);
    
    public BuyDetailBean getBuyDetailById(Long id);
    
    public Long newBuyDetail(BuyDetailBean sale);
    
    public List<BuyDetailBean> getAllBuyDetails();
    
    public Long updateBuyDetail(BuyDetailBean sale);
    
    public void deleteBuyDetail(Long id);
}