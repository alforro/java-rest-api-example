package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.SessionBean;
import py.pol.una.ii.pw.mybatis.bean.UserBean;
import py.pol.una.ii.pw.mybatis.mapper.SessionMapper;
import py.pol.una.ii.pw.mybatis.mapper.UserMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class UserManager implements UserMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    @Inject
    private Logger log;
    
    
    private SqlSession sqlSession;
    
    @EJB
    private SessionManager sessionManager;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            exist = userMapper.getUserById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newUser(UserBean user) {
        
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            
            userMapper.newUser(user);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return user.getId();
    }
    public UserBean getUserById(Long id) {
        
    	UserBean user = null;
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            user = userMapper.getUserById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (user);
    }
    
    public UserBean getUserByUsername(String username) {
        
    	UserBean user = null;
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            user = userMapper.getUserByUsername(username);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (user);
    }
   
    public List<UserBean> findAllOrderedByName(){
        
        List<UserBean> users = null;
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            users = userMapper.getAllUsers();
            
            //Collections.sort(clientes);//, (a, b) -> a.getName().compareToIgnoreCase(b.getName()));
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (users);
    }
    public List<UserBean> getAllUsers(){
        
        List<UserBean> users = null;
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            users = userMapper.getAllUsers();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (users);
    }
    public Long updateUser(UserBean user_mod){
        
        Long id_user = null;
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            id_user = userMapper.updateUser(user_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_user;
    }
    
    public void deleteUser(Long id){
        try {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            userMapper.deleteUser(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
  
    public SessionBean login(String username, String password){
    	
    	UserBean user = null;
        Long sessionId = null;
        SessionBean session = null;
        
        try {
            UserMapper usuarioMapper = sqlSession.getMapper(UserMapper.class);
            user = usuarioMapper.getUserByUsername(username);
            if(user.getPassword().equals(password)){
            	
            	SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
            	session = new SessionBean();            	
            	session.setToken(user.getUsername());
            	sessionId =  sessionMapper.newSession(session);
            	log.info("Registering " + sessionId);
            	return session;
            }
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
      
        return session;
    }
    
    public void logout(String token){
                
        try {
            
        	SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
        	
            SessionBean session = sessionMapper.getSessionByToken(token);
            
            //Borrar sesión al hacer logout
            sessionMapper.deleteSession(session.getId());
            
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
}
