/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.SessionBean;
import py.pol.una.ii.pw.mybatis.bean.UserBean;
import py.pol.una.ii.pw.mybatis.manager.SessionManager;
import py.pol.una.ii.pw.mybatis.manager.UserManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class UserRegistration {

    @Inject
    private Logger log;
    
    @EJB
    private UserManager userManager;
    
    @EJB
    private SessionManager sessionManager;
    
    /**
     * Crear un nuevo usuario
     * 
     * @param user
     * @return void
     * 
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createUser(UserBean user) throws Exception {
    	
        log.info("Registrando usuario " + user.getUsername());

    	
        UserBean newUser = new UserBean();
    	
    	newUser.setPassword(user.getPassword());
    	newUser.setUsername(user.getUsername());
    	
        userManager.newUser(newUser);
        
    } 
    
    /**
     * Eliminar un usuario existente
     * 
     * @param id
     * @return void
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteUser(Long id){
    	
    	  log.info("Eliminar usuario con id" + id);
    	  
    	  userManager.deleteUser(id);
    	  
    	  log.info("Se ha eliminado usuario con id " + id);
    }
    
    /**
     * Listar todos los usuarios existentes
     * 
     * @return list 
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<UserBean> findAll() {
        
    	log.info("Buscando todos los usuarios existentes");
        
        List<UserBean> list = userManager.getAllUsers();
        
        log.info("Se han encontrado " + list.size() + " usuarios");
        return list;
    }
    
    /**
     * Buscar usuario por id
     * 
     * @param id
     * @return user
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public UserBean findById(Long id) {
        
    	log.info("Buscando usuario con id " + id);
    	
    	UserBean user = userManager.getUserById(id);
    	
    	log.info("Se ha encontrado el usuario con id " + user.getId());

        return user;
    }
    
    /**
     * Buscar usuario por nombre de usuario
     * 
     * @param username
     * @return UserBean
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public UserBean findByUsername(String username) {
    	
        log.info("Buscando usuario con username " + username);
        
        UserBean user = userManager.getUserByUsername(username);

        log.info("Se ha encontrado usuario con username " + user.getUsername());
        
        return user; 
    }

    /**
     * Loguear a usuario existente
     * 
     * @param username
     * @param password
     * @return SessionBean
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SessionBean login(String username, String password) {
        
    	log.info("Logueando usuario: " + username);
        
    	SessionBean session = userManager.login(username, password);
        
        log.info("Se logueo al usuario con: " + username);
        
        return session;
    }

    /**
     * Desloquear usuario existente
     * 
     * @param token
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void logout(String token) {
        
    	log.info("Desauntenticando usuario: " + token);
        
    	userManager.logout(token);
        
    	log.info("Se desauntentico al usuario con el token: " + token);
    }
}