/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.ClientBean;
import py.pol.una.ii.pw.mybatis.bean.ProductBean;
import py.pol.una.ii.pw.mybatis.bean.SaleBean;
import py.pol.una.ii.pw.mybatis.bean.SaleDetailBean;
import py.pol.una.ii.pw.mybatis.manager.ClientManager;
import py.pol.una.ii.pw.mybatis.manager.ProductManager;
import py.pol.una.ii.pw.mybatis.manager.SaleDetailManager;
import py.pol.una.ii.pw.mybatis.manager.SaleManager;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import javax.ejb.TransactionManagementType;

import javax.transaction.HeuristicMixedException;

import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;

import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

/**
 * 
 * @author gmarin
 *
 */

@Stateful
@SessionScoped
@TransactionManagement(TransactionManagementType.BEAN)
public class SaleRegistration {
	
	@Resource
	private EJBContext context;

	@Resource
	private TransactionManager transaction;
	
	@Inject
    private Logger log;
    
    @EJB
    private SaleManager saleManager;
    
    @EJB
    private SaleDetailManager saleDetailManager;
    
    @EJB
    private ProductManager productManager;
    
    @EJB
    private ClientManager clientManager;
    
    private Transaction ts;
    
    private SaleBean sl = new SaleBean();

    /**
     * Iniciar venta por cliente
     * 
     * @param client
     */
	public void initSale(ClientBean client){
		
		try {
			transaction.begin();
	    	
	    	this.sl.setDate(new Date());
	    	this.sl.setTotal(0.0F);
	    	this.sl.setClientId(client.getId());
	        
	    	saleManager.newSale(sl);

		} catch (NotSupportedException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();
			
		}
		
		try {
			ts = transaction.suspend();
		} catch (SystemException e) {
			e.printStackTrace();
			
		}
	}
    
	/**
	 * Agregar items a la venta
	 * 
	 * @param saleDetailList
	 * @throws Exception
	 */
    public void addItemSale(List<SaleDetailBean> saleDetailList) throws Exception{
    	
    	Float total = 0.0F;
    	
    	transaction.resume(ts);
    	
    	
    	for(SaleDetailBean saleDetail: saleDetailList){

            log.info("Registrando detalle de venta con idProducto " + saleDetail.getProductId());
            
            SaleDetailBean newSaleDetail = new SaleDetailBean();
            
            newSaleDetail.setProductId(saleDetail.getProductId());
            newSaleDetail.setQuantity(saleDetail.getQuantity());
            newSaleDetail.setSaleId(this.sl.getId());
            
        	saleDetailManager.newSaleDetail(newSaleDetail);
            
        	ProductBean product= productManager.getProductById(newSaleDetail.getProductId());
        	
        	Integer quantity = product.getStock() - newSaleDetail.getQuantity();
        	total = (float) (total +(newSaleDetail.getQuantity()*product.getSalePrice()));
        	
        	product.setStock(quantity);
        	                   
        }
    	
    	this.sl.setTotal(total);
    	
    	ClientBean cl = clientManager.getClientById(sl.getClientId());
    	
    	Float newDue = cl.getDue()+total;
    	
    	cl.setDue(newDue);
    	
    	clientManager.updateClient(cl);
    	saleManager.updateSale(sl);
    	
    	ts = transaction.suspend();
    }
    
    /**
     * Eliminar items de la venta
     * 
     * @param idDetailSales
     * @throws Exception
     */
    public void deleteItemSale(List<Long> idDetailSales) throws Exception{
    	    	
    	transaction.resume(ts);
    	
    	Float total = this.sl.getTotal();
    	
    	for(Long idSaleDetail: idDetailSales){

            log.info("Eliminando item con id " + idSaleDetail);
          
            SaleDetailBean sd = saleDetailManager.getSaleDetailById(idSaleDetail);
            
            ProductBean product= productManager.getProductById(sd.getProductId());
        	
        	Integer quantity = product.getStock() + sd.getQuantity();
        	total = (float) (total -(sd.getQuantity()*product.getSalePrice()));
        	
        	product.setStock(quantity);
        	
      	    productManager.updateProduct(product);
      	    
      	    saleManager.deleteSale(idSaleDetail);
        	                   
        }
    	    	    	    	
    	this.sl.setTotal(total);
    	
    	ClientBean cl = clientManager.getClientById(sl.getClientId());
    	
    	Float newDue = cl.getDue() - total;
    	
    	cl.setDue(newDue);
    	
    	clientManager.updateClient(cl);
    	saleManager.updateSale(sl);
    	
    	ts = transaction.suspend();
    	
    }
    
    /**
     * Confirmar venta
     * 
     * @return sl
     */
    public SaleBean confirmSale(){
    	try {
			transaction.resume(ts);
			
		} catch (InvalidTransactionException e) {
			e.printStackTrace();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();
			
		}
    	
    	try {
        	saleManager.updateSale(sl);
			transaction.commit();
			
		} catch (SecurityException e) {
			e.printStackTrace();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
		} catch (RollbackException e) {
			e.printStackTrace();
			
		} catch (HeuristicMixedException e) {
			e.printStackTrace();
			
		} catch (HeuristicRollbackException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
    	
    	return this.sl;
    	
    }
    
    /**
     * Cancelar venta
     */
    public void cancelSale(){
    	try {
			transaction.resume(ts);
			
		} catch (InvalidTransactionException e) {
			e.printStackTrace();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();
			
		}
    	
    	try {
			transaction.rollback();
		} catch (SecurityException e) {
			e.printStackTrace();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			
		} catch (SystemException e) {
			e.printStackTrace();
			
		}
    	
    }
    
    /**
     * Actualizar datos de venta
     * 
     * @param sale_mod
     */
    public void updateSale(SaleBean sale_mod) {
    	
    	log.info("Actualizando venta con id " + sale_mod.getId());
    	
    	SaleBean sale = saleManager.getSaleById(sale_mod.getId());
    	
    	sale.setClientId(sale_mod.getClientId());
    	sale.setDate(sale_mod.getDate());
    	sale.setTotal(sale_mod.getTotal());
    	
    	saleManager.updateSale(sale);
    	
    	log.info("Se ha actualizado venta con id " + sale.getId());
    }
 
    @PostConstruct
    public void postConstruct() throws NamingException{
	
    	InitialContext ctx = new InitialContext();
		try {
			transaction= (TransactionManager) ctx.lookup("java:/TransactionManager");
		
		} catch (Exception e) {
		// TODO: handle exception
		}
    }
    
}