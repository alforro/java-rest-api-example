/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.pol.una.ii.pw.serviceImpl;

import py.pol.una.ii.pw.mybatis.bean.ProductBean;
import py.pol.una.ii.pw.mybatis.bean.SaleDetailBean;
import py.pol.una.ii.pw.mybatis.manager.ProductManager;
import py.pol.una.ii.pw.mybatis.manager.SaleDetailManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import java.util.List;
import java.util.logging.Logger;

/**
 * 
 * @author gmarin
 *
 */
// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class SaleDetailRegistration {

    @Inject
    private Logger log;
    
    @EJB
    private SaleDetailManager saleDetailManager;
    
    @EJB
    private ProductManager productManager;
    
    /**
     * Crear nuevo detalle de venta
     * 
     * @param saleDetail
     * @throws Exception
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createSaleDetail(SaleDetailBean saleDetail) throws Exception {
        
    	log.info("Registering " + saleDetail.getId());
    	
    	SaleDetailBean newSaleDetail = new SaleDetailBean();
    	
    	validateProductStock(saleDetail);
    	
    	newSaleDetail.setProductId(saleDetail.getProductId());
    	newSaleDetail.setQuantity(saleDetail.getQuantity());
    	newSaleDetail.setSaleId(saleDetail.getSaleId());

    	Long saleDetailId = saleDetailManager.newSaleDetail(newSaleDetail);
    	
    	log.info("Se ha registrado el detalle de venta con id " + saleDetailId);
    	
    }
   
    /**
     * Validación de producto en stock. 
     * Verifica que la cantidad vendida es inferior a la cantidad registrada en el stock.
     * 
     * @param saleDetail
     * @throws Exception
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private void validateProductStock(SaleDetailBean saleDetail) throws Exception{
    	
    	ProductBean product = productManager.getProductById(saleDetail.getProductId());
    	
    	if (product.getStock()<saleDetail.getQuantity()){
    		
    		throw new Exception("La cantidad vendida supera al stock actual");
    		
    	}
    }
    
    /**
     * Actualizar un detalle de venta
     * 
     * @param saleDetail
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateSaleDetail(SaleDetailBean saleDetail){
    	
    	log.info("Update sale detail with id " + saleDetail.getId());
    	
    	SaleDetailBean newSaleDetail = saleDetailManager.getSaleDetailById(saleDetail.getId());
    	
    	newSaleDetail.setProductId(saleDetail.getProductId());
    	newSaleDetail.setQuantity(saleDetail.getQuantity());
    	newSaleDetail.setSaleId(saleDetail.getSaleId());
    	
    	saleDetailManager.updateSaleDetail(newSaleDetail);
	
    	log.info("Se ha actualizado detalle de venta con id " + newSaleDetail.getId());

    }
    
    /**
     * Listar todos los detalles de ventas existentes
     * 
     * @return list
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<SaleDetailBean> findAll(){
    	
    	log.info("Buscando todos los detalles de venta");
    	
    	List<SaleDetailBean> list = saleDetailManager.getAllSaleDetails();
    	
    	log.info("Se han obtenido " + list.size() + "detalles de venta.");
    	
    	return list;
    }
    
    /**
     * Buscar detalle de venta por id
     * 
     * @param id
     * @return detail
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaleDetailBean findById(Long id){
    	
    	log.info("Buscando detalle de venta con id " + id);
    	
    	SaleDetailBean detail = saleDetailManager.getSaleDetailById(id);
    	
    	log.info("Se ha obtenido detalle de venta con id " + detail.getId());
    	
    	return detail;
    }
    
    /**
     * Eliminar detalle de venta por id
     * 
     * @param id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteSaleDetail(Long id){
    	
    	log.info("Eliminando detalle de venta con id "+ id);
    	
    	saleDetailManager.deleteSaleDetail(id);
    	
    	log.info("Se ha eliminado detalle de venta con id "+ id);

    }
    
}