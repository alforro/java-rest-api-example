package py.pol.una.ii.pw.mybatis.manager;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;

import py.pol.una.ii.pw.mybatis.bean.SaleBean;
import py.pol.una.ii.pw.mybatis.mapper.SaleMapper;
import py.pol.una.ii.pw.util.ConnectionFactory;

@Stateless
@LocalBean
public class SaleManager implements SaleMapper {
    
    @EJB
    private ConnectionFactory connectionFactory;
    
    private SqlSession sqlSession;
    
    @PostConstruct
    void init(){
        sqlSession = connectionFactory.getSqlSessionFactory();
    }
    public Boolean isExist(Long id) {
        
        Boolean exist = false;
        try {
        	SaleMapper saleMapper = sqlSession.getMapper(SaleMapper.class);
            exist = saleMapper.getSaleById(id) == null;
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return exist;
    }
    public Long newSale(SaleBean sale) {
        
        try {
        	SaleMapper saleMapper = sqlSession.getMapper(SaleMapper.class);
            
        	saleMapper.newSale(sale);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            System.out.println("Existio un error al ejecutar la instruccion con la base de datos: " + e.toString());
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return sale.getId();
    }
    public SaleBean getSaleById(Long id) {
        
    	SaleBean sale = null;
        try {
        	SaleMapper saleMapper = sqlSession.getMapper(SaleMapper.class);
        	sale = saleMapper.getSaleById(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (sale);
    }
   
    public List<SaleBean> getAllSales(){
        
        List<SaleBean> sales = null;
        try {
        	SaleMapper saleMapper = sqlSession.getMapper(SaleMapper.class);
            sales = saleMapper.getAllSales();
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return (sales);
    }
    public Long updateSale(SaleBean sale_mod){
        
        Long id_sale = null;
        try {
        	SaleMapper saleMapper = sqlSession.getMapper(SaleMapper.class);
            id_sale = saleMapper.updateSale(sale_mod);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
        return id_sale;
    }
    
    public void deleteSale(Long id){
        try {
        	SaleMapper saleMapper = sqlSession.getMapper(SaleMapper.class);
        	saleMapper.deleteSale(id);
        } catch (Exception e) {
            System.out.println("Existio un error al ejecutar la instruccion sql: " + e.getMessage()
                    + " ##### Con nombre: " + e.getClass().getName());
        }
    }
    
    @PreDestroy
    public void close(){
        connectionFactory.close();
    }
	
}
